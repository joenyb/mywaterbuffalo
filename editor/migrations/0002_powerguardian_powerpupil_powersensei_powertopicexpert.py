# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-01-30 17:18
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('editor', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='PowerGuardian',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('strength', models.IntegerField(default=1)),
                ('assigned', models.DateTimeField()),
                ('validated', models.BooleanField(default=False)),
                ('editor', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='editor.Editor')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='PowerPupil',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('strength', models.IntegerField(default=1)),
                ('assigned', models.DateTimeField()),
                ('validated', models.BooleanField(default=False)),
                ('editor', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='editor.Editor')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='PowerSensei',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('strength', models.IntegerField(default=1)),
                ('assigned', models.DateTimeField()),
                ('validated', models.BooleanField(default=False)),
                ('editor', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='editor.Editor')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='PowerTopicExpert',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('strength', models.IntegerField(default=1)),
                ('assigned', models.DateTimeField()),
                ('validated', models.BooleanField(default=False)),
                ('editor', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='editor.Editor')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
